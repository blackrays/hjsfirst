//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    msg: "新加的消息",
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    wx.getLocation({
      type: 'wgs84',  // wgs84代表gps
      success: (res) => {
        var latitude = res.latitude // 纬度
        var longitude = res.longitude // 经度
        this.setData({
          latitude: latitude,
          longitude: longitude
        })
      }
    })
    
    if (app.globalData.userInfo) {
      console.log(11111)
      console.log(app.globalData.userInfo)
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        console.log(22222)
        console.log(res.userInfo)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          console.log(333333)
          console.log(res.userInfo)
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  //扫码
  startScan: function(e) {
    wx.scanCode({
      success: (res) => {
        console.log(res)
        this.setData({
          scanContent: res.result
        })
      }
    })
  },
  invokeInter: function(e){
    wx.request({
      url: 'http://www.badiu.com',
      type: 'GET',
      success: (res) => {
        console.log(res)
        this.setData({
          invokeContent: res.errMsg
        });
      }
    })
  },
  getloginInfo: function(e){
    wx.login({
      success: function (res) {
        console.log('loginCode:', res.code)
        wx.request({
          url: 'https://api.weixin.qq.com/sns/jscode2session?appid=wx6837e48b48655be0&secret=2a17234dcdb37e4c82beb4292a333fb9&js_code='+res.code+'&grant_type=authorization_code',
          success: (res) => {
            console.log(res);
          }
        })
      }
    })
  },
  // 小程序主题不是企业无法获得手机号
  getPhoneNumber: function(e){
    console.log(e)
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    console.log(e.detail.getPhoneNumber)
  },
  bindPacklist: function(e){
    wx.navigateTo({
      url: '../packlist/packlist',
    })
  }
})
