//logs.js
const util = require('../../utils/util.js')
const app = getApp()

Page({
  data: {
    userinfo: {},
    logs: []
  },
  onLoad: function () {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return util.formatTime(new Date(log))
      })
    })

    if (app.globalData.userInfo) {
      this.setData({
        //需要在onLoad及其后生命周期下才能获取到全局变量
        userInfo: app.globalData.userInfo
      })
    }else{
      this.setData({
        userInfo: "no login info"
      })
    }
  }
})
